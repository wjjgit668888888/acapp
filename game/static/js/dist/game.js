class AcGameMenu {
    constructor(root) {
        this.root = root;
        this.$menu = $(`

<div class="ac-game-menu">
    <div class="ac-game-menu-field">
        <div class="ac-game-menu-field-item ac-game-menu-field-item-single-mode">
            单人模式
        </div>
        <br>
        <div class="ac-game-menu-field-item ac-game-menu-field-item-multi-mode">
            多人模式
        </div>
        <br>
        <div class="ac-game-menu-field-item ac-game-menu-field-item-settings">
            退出
        </div>
    </div>
</div>


`);
        this.$menu.hide();
        this.root.$ac_game.append(this.$menu);
        this.$single_mode = this.$menu.find('.ac-game-menu-field-item-single-mode');
        this.$multi_mode = this.$menu.find('.ac-game-menu-field-item-multi-mode');
        this.$settings = this.$menu.find('.ac-game-menu-field-item-settings');

        this.start();
    }
    
    start() {
        this.add_listening_events();
    }
    
    add_listening_events() {
        let outer = this;
        this.$single_mode.click(function(){
            outer.hide();
            outer.root.playground.show("single mode");
        });
        this.$multi_mode.click(function(){
            outer.hide();
            outer.root.playground.show("multi mode");
        });
        this.$settings.click(function(){
            console.log("click settings");
            outer.root.settings.logout_on_remote();
        });
    }
    
    show() {  // 显示menu界面
        this.$menu.show();
    }
    
    hide() {  // 关闭menu界面
        this.$menu.hide();
    }

}
let AC_GAME_OBJECTS = []; // 储存所有可以“动”的元素的全局数组

class AcGameObject
{
    constructor(hurtable = false) // 构造函数
    {
        AC_GAME_OBJECTS.push(this);  // 将这个对象加入到储存动元素的全局数组里

        this.has_call_start = false; // 记录这个对象是否已经调用了start函数
        this.timedelta = 0; // 当前距离上一帧的时间间隔，相等于时间微分，用来防止因为不同浏览器不同的帧数，物体移动若按帧算会不同，所以要用统一的标准，就要用时间来衡量

        this.hurtable = hurtable; // 决定这个元素能否被碰撞，默认为不能
        this.uuid = this.create_uuid();
		console.log(this.uuid);
    }
	create_uuid() {
        let res = "";
        for (let i = 0; i < 8; i ++ ) {
            let x = parseInt(Math.floor(Math.random() * 10));   //[0, 10)
            res += x;
        }
        return res;
	}

    start()
    {
        // 只会在第一帧执行一次的过程
    }

    update()
    {
        // 每一帧都会执行的过程
    }

    destroy()
    {
        this.on_destroy();
        // 删除这个元素
        for (let i = 0; i < AC_GAME_OBJECTS.length; ++ i)
        {
            if (AC_GAME_OBJECTS[i] === this)
             {
                 AC_GAME_OBJECTS.splice(i, 1); // 从数组中删除元素的函数splice()
                 break;
             }
        }
    }

    on_destroy()
    {
        // 被删除之前的过程，“临终遗言”
    }
}

let last_timestp; // 上一帧的时间
let AC_GAME_ANIMATION = function(timestp) // timestp 是传入的一个参数，就是当前调用的时间
{
    for (let i = 0; i < AC_GAME_OBJECTS.length; ++ i) // 所有动的元素都进行更新。
    {
        let obj = AC_GAME_OBJECTS[i];
        if (!obj.has_called_start)
        {
            obj.start(); // 调用start()
            obj.has_called_start = true; // 表示已经调用过start()了
        }
        else 
        {
            obj.timedelta = timestp - last_timestp; // 时间微分
            obj.update(); // 不断调用
        }
    }
    last_timestp = timestp; // 进入下一帧时当前时间戳就是这一帧的时间戳

    requestAnimationFrame(AC_GAME_ANIMATION); // 不断递归调用
}

requestAnimationFrame(AC_GAME_ANIMATION); // JS的API，可以调用1帧里面的函数。(有些浏览器的一秒帧数不一样）
class GameMap extends AcGameObject
{
    constructor(playground)
    {
        super(); // 调用基类的构造函数

        this.playground = playground; // 这个Map是属于这个playground的
        this.$canvas = $(`<canvas></canvas>`); // canvas是画布
        this.ctx = this.$canvas[0].getContext('2d'); // 用ctx操作画布canvas

        this.ctx.canvas.width = this.playground.width; // 设置画布的宽度
        this.ctx.canvas.height = this.playground.height; // 设置画布的高度

        this.playground.$playground.append(this.$canvas); // 将这个画布加入到这个playground
    }

    render()
    {
        this.ctx.fillStyle = "rgba(0, 0, 0, 0.2)"; // 填充颜色设置为透明的黑色,方便显示小球
        this.ctx.fillRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height); // 画上给定的坐标的矩形
    }

    start()
    {

    }

	resize() {
        this.ctx.canvas.width = this.playground.width;
        this.ctx.canvas.height = this.playground.height;
		this.ctx.fillStyle = "rgba(0, 0, 0, 1)";    // resize 完，涂一层不透明的即可
        this.ctx.fillRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
    }

    update()
    {
        this.render(); // 这个地图要一直画一直画（动画的基本原理）
    }

}


class Particle extends AcGameObject {
    constructor(playground, x, y, radius, vx, vy, color, speed, move_length) {
        super();
        this.playground = playground;
        this.ctx = this.playground.game_map.ctx;
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.vx = vx;
        this.vy = vy;
        this.color = color;
        this.speed = speed;
        this.move_length = move_length;
        this.friction = 0.9;
        this.eps = 0.01;
    }

    start() {
    }

    update() {
        if (this.move_length < this.eps || this.speed < this.eps) {
            this.destroy();
            return false;
        }

        let moved = Math.min(this.move_length, this.speed * this.timedelta / 1000);
        this.x += this.vx * moved;
        this.y += this.vy * moved;
        this.speed *= this.friction;
        this.move_length -= moved;
        this.render();
    }

    render() {
        let scale = this.playground.scale;
        this.ctx.beginPath();
        this.ctx.arc(this.x * scale, this.y * scale, this.radius * scale, 0, 2 * Math.PI, false);
        this.ctx.fillStyle = this.color;
        this.ctx.fill();
    }
}

class Player extends AcGameObject {
    constructor(playground, x, y, radius, color, speed, character, username, photo) {
        super();
        this.playground = playground;
        this.ctx = this.playground.game_map.ctx;
        this.x = x;
        this.y = y;
        this.vx = 0;
        this.vy = 0;
        this.damage_x = 0;
        this.damage_y = 0;
        this.damage_speed = 0;
        this.move_length = 0;
        this.radius = radius;
        this.color = color;
        this.speed = speed;
        this.character = character;
        this.username = username;
        this.photo = photo;

        this.eps = 0.01;
        this.friction = 0.9;
        this.spent_time = 0;

        console.log(color);

        this.cur_skill = null;

        if (this.character !== "robot") // 如果这是自己
    	{
        	this.img = new Image(); // 头像的图片
        	this.img.src = this.playground.root.settings.photo; // 头像的图片的URL
    	}
    }

    start() {
        if (this.character == "me") {
            this.add_listening_events();
        } else {
            let tx = Math.random() * this.playground.width / this.playground.scale;
            let ty = Math.random() * this.playground.height / this.playground.scale;
            this.move_to(tx, ty);
        }
    }

    add_listening_events() {
        let outer = this;
        this.playground.game_map.$canvas.on("contextmenu", function() {
            return false;
        });
        this.playground.game_map.$canvas.mousedown(function(e) {
            const rect = outer.ctx.canvas.getBoundingClientRect();
            if (e.which === 3) {
                outer.move_to((e.clientX - rect.left) / outer.playground.scale, (e.clientY - rect.top) / outer.playground.scale);
            } else if (e.which === 1) {
                if (outer.cur_skill === "fireball") {
                    outer.shoot_fireball((e.clientX - rect.left) / outer.playground.scale, (e.clientY - rect.top) / outer.playground.scale);
                }

                outer.cur_skill = null;
            }
        });

        $(window).keydown(function(e) {
            if (e.which === 81) {  // q
                outer.cur_skill = "fireball";
                return false;
            }
        });
    }

    shoot_fireball(tx, ty) {
        let x = this.x, y = this.y;
        let radius = 0.01;
        let angle = Math.atan2(ty - this.y, tx - this.x);
        let vx = Math.cos(angle), vy = Math.sin(angle);
        let color = "orange";
        let speed = 0.5;
        let move_length = 1.0;
        let damage = 0.01;
        new FireBall(this.playground, this, x, y, radius, vx, vy, color, speed, move_length, damage);
    }

    get_dist(x1, y1, x2, y2) {
        let dx = x1 - x2;
        let dy = y1 - y2;
        return Math.sqrt(dx * dx + dy * dy);
    }

    move_to(tx, ty) {
        this.move_length = this.get_dist(this.x, this.y, tx, ty);
        let angle = Math.atan2(ty - this.y, tx - this.x);
        this.vx = Math.cos(angle);
        this.vy = Math.sin(angle);
    }

    is_attacked(angle, damage) {
        for (let i = 0; i < 20 + Math.random() * 10; i ++ ) {
            let x = this.x, y = this.y;
            let radius = this.radius * Math.random() * 0.1;
            let angle = Math.PI * 2 * Math.random();
            let vx = Math.cos(angle), vy = Math.sin(angle);
            let color = this.color;
            let speed = this.speed * 10;
            let move_length = this.radius * Math.random() * 5;
            new Particle(this.playground, x, y, radius, vx, vy, color, speed, move_length);
        }
        this.radius -= damage;
        if (this.radius < this.eps) {
            this.destroy();
            return false;
        }
        this.damage_x = Math.cos(angle);
        this.damage_y = Math.sin(angle);
        this.damage_speed = damage * 100;
        this.speed *= 0.8;
    }

    update() {
        this.update_move();
        this.render();
    }

    update_move() {

        this.spent_time += this.timedelta / 1000;
        if (this.character == "robot" && this.spent_time > 4 && Math.random() < 1 / 300.0) {
            let player = this.playground.players[Math.floor(Math.random() * this.playground.players.length)];
            let tx = player.x + player.speed * this.vx * this.timedelta / 1000 * 0.3;
            let ty = player.y + player.speed * this.vy * this.timedelta / 1000 * 0.3;
            this.shoot_fireball(tx, ty);
        }

        if (this.damage_speed > this.eps) {
            this.vx = this.vy = 0;
            this.move_length = 0;
            this.x += this.damage_x * this.damage_speed * this.timedelta / 1000;
            this.y += this.damage_y * this.damage_speed * this.timedelta / 1000;
            this.damage_speed *= this.friction;
        } else {
            if (this.move_length < this.eps) {
                this.move_length = 0;
                this.vx = this.vy = 0;
                if (this.character == "robot") {
                    let tx = Math.random() * this.playground.width / this.playground.scale;
                    let ty = Math.random() * this.playground.height / this.playground.scale;
                    this.move_to(tx, ty);
                }
            } else {
                let moved = Math.min(this.move_length, this.speed * this.timedelta / 1000);
                this.x += this.vx * moved;
                this.y += this.vy * moved;
                this.move_length -= moved;
            }
        }
    }

    render() {
        let scale = this.playground.scale;
		if (this.character !== "robot") {
            this.ctx.save();
            this.ctx.beginPath();
            this.ctx.arc(this.x * scale, this.y * scale, this.radius * scale, 0, Math.PI * 2, false);
            this.ctx.stroke();
            this.ctx.clip();
            this.ctx.drawImage(this.img, (this.x - this.radius) * scale, (this.y - this.radius) * scale, this.radius * 2 * scale, this.radius * 2 * scale);
            this.ctx.restore();
        } else {
            this.ctx.beginPath();
            this.ctx.arc(this.x * scale, this.y * scale, this.radius * scale, 0, 2 * Math.PI, false);
            this.ctx.fillStyle = this.color;
            this.ctx.fill();
        }
    }

    on_destroy() {
        for (let i = 0; i < this.playground.players.length; i ++ ) {
            if (this.playground.players[i] === this) {
                this.playground.players.splice(i, 1);
            }
        }
    }
}

class FireBall extends AcGameObject {
    constructor(playground, player, x, y, radius, vx, vy, color, speed, move_length, damage) {
        super();
        this.playground = playground;
        this.player = player;
        this.ctx = this.playground.game_map.ctx;
        this.x = x;
        this.y = y;
        this.vx = vx;
        this.vy = vy;
        this.radius = radius;
        this.color = color;
        this.speed = speed;
        this.move_length = move_length;
        this.damage = damage;
        this.eps = 0.01;
    }

    start() {
    }

    update() {
        if (this.move_length < this.eps) {
            this.destroy();
            return false;
        }

        let moved = Math.min(this.move_length, this.speed * this.timedelta / 1000);
        this.x += this.vx * moved;
        this.y += this.vy * moved;
        this.move_length -= moved;

        for (let i = 0; i < this.playground.players.length; i ++ ) {
            let player = this.playground.players[i];
            if (this.player !== player && this.is_collision(player)) {
                this.attack(player);
            }
        }

        this.render();
    }

    get_dist(x1, y1, x2, y2) {
        let dx = x1 - x2;
        let dy = y1 - y2;
        return Math.sqrt(dx * dx + dy * dy);
    }

    is_collision(player) {
        let distance = this.get_dist(this.x, this.y, player.x, player.y);
        if (distance < this.radius + player.radius)
            return true;
        return false;
    }

    attack(player) {
        let angle = Math.atan2(player.y - this.y, player.x - this.x);
        player.is_attacked(angle, this.damage);
        this.destroy();
    }

    render() {
        let scale = this.playground.scale;
        this.ctx.beginPath();
        this.ctx.arc(this.x * scale, this.y * scale, this.radius * scale, 0, 2 * Math.PI, false);
        this.ctx.fillStyle = this.color;
        this.ctx.fill();
    }
}

class MultiPlayerSocket {
    constructor(playground) {
        this.playground = playground;
        this.ws = new WebSocket("wss://app6560.acapp.acwing.com.cn/wss/multiplayer/");
        this.start();
    }
    start() {
        this.receive();
    }
	receive() {
        let outer = this;
        this.ws.onmessage = function(e) {
            let data = JSON.parse(e.data);
            let uuid = data.uuid;
            if (uuid === outer.uuid) return false;

            let event = data.event;
            if (event === "create_player") {
                outer.receive_create_player(uuid, data.username, data.photo);
            }
        };
    }
	send_create_player(username, photo) {
        let outer = this;
        this.ws.send(JSON.stringify({
            'event': 'create_player',
            'uuid': outer.uuid,
            'username': username,
            'photo': photo,
        }));
    }

    receive_create_player(uuid, username, photo) {
        let player = new Player(
            this.playground,
            this.playground.width / 2 / this.playground.scale,
            0.5,
            0.05,
            "white",
            0.15,
            "enemy",
            username,
            photo,
        );
        player.uuid = uuid;
        this.playground.players.push(player);
    }
}
class AcGamePlayground {
    constructor(root) {
        this.root = root;
        this.$playground = $(`<div class="ac-game-playground"></div>`);

        this.hide();

        this.root.$ac_game.append(this.$playground);// 未来可能会多次 show 因此把创建场景挪到这里

        this.start();
    }

    get_random_color() {
        let colors = ["blue", "red", "pink", "grey", "green"];
        return colors[Math.floor(Math.random() * 5)];
    }

    start() {
		let outer = this;
        $(window).resize(function() {
            outer.resize();
        });
    }
	resize() {
        this.width = this.$playground.width();
        this.height = this.$playground.height();
        let unit = Math.min(this.width / 16, this.height / 9);  // 以最小的作为基准，渲染
        this.width = unit * 16;
        this.height = unit * 9;
        this.scale = this.height;   // resize时，其他元素的渲染大小都以当前渲染的高度为基准，存为 scale 变量

        if (this.game_map) this.game_map.resize();  //如果此时地图已创建，则resize一下
    }

    show(mode) {  // 打开playground界面
        this.$playground.show();
        
        this.width = this.$playground.width();
        this.height = this.$playground.height();
        this.game_map = new GameMap(this);

		this.resize();

        this.players = [];
        this.players.push(new Player(this, this.width / 2 / this.scale, 0.5, 0.05, "white", 0.15, "me", this.root.settings.username, this.root.settings.photo));
		if (mode === "single mode") {
            for (let i = 0; i < 5; i ++ ) {
                this.players.push(new Player(this, this.width / 2 / this.scale, 0.5, 0.05, this.get_random_color(), 0.15, "robot"));
            }
        } else if (mode === "multi mode") {
			this.mps = new MultiPlayerSocket(this);
            let outer = this;
            this.mps.uuid = this.players[0].uuid;
            this.mps.ws.onopen = function() {
                outer.mps.send_create_player(outer.root.settings.username, outer.root.settings.photo);
            };
        }
        
    }

    hide() {  // 关闭playground界面
        this.$playground.hide();
    }
}

class Settings {
	constructor(root) {
		this.root = root;
		this.platform = "WEB";
		if (this.root.AcWingOS) this.platform = "ACAPP";
		this.username = "";
		this.photo = "";

		this.$settings = $(`
<div class="ac-game-settings">
	<div class="ac-game-settings-login">
		<div class="ac-game-settings-title">
			登录
		</div>
		<div class="ac-game-settings-username">
			<div class="ac-game-settings-item">
				<input type="text" placeholder="用户名">
			</div>
		</div>
		<div class="ac-game-settings-password">
			<div class="ac-game-settings-item">
				<input type="password" placeholder="密码">
			</div>
		</div>
		<div class="ac-game-settings-submit">
			<div class="ac-game-settings-item">
				<button>登录</button>
			</div>
		</div>
		<div class="ac-game-settings-error-message">
		</div>
		<div class="ac-game-settings-option">
			注册
		</div>
		<br>
		<div class="ac-game-settings-acwing">
			<img width="30" src="https://app165.acapp.acwing.com.cn/static/image/settings/acwing_logo.png">
			<br>
			<div>
				AcWing一键登录
			</div>
		</div>
	</div>
	<div class="ac-game-settings-register">
		<div class="ac-game-settings-title">
			注册
		</div>
		<div class="ac-game-settings-username">
			<div class="ac-game-settings-item">
				<input type="text" placeholder="用户名">
			</div>
		</div>
		<div class="ac-game-settings-password ac-game-settings-password-first">
			<div class="ac-game-settings-item">
				<input type="password" placeholder="密码">
			</div>
		</div>
		<div class="ac-game-settings-password ac-game-settings-password-second">
			<div class="ac-game-settings-item">
				<input type="password" placeholder="确认密码">
			</div>
		</div>
		<div class="ac-game-settings-submit">
			<div class="ac-game-settings-item">
				<button>注册</button>
			</div>
		</div>
		<div class="ac-game-settings-error-message">
		</div>
		<div class="ac-game-settings-option">
			登录
		</div>
		<br>
		<div class="ac-game-settings-acwing">
			<img width="30" src="https://app165.acapp.acwing.com.cn/static/image/settings/acwing_logo.png">
			<br>
			<div>
				AcWing一键登录
			</div>
		</div>
	</div>
</div>
`);


		this.$login = this.$settings.find(".ac-game-settings-login");
		this.$login_username = this.$login.find(".ac-game-settings-username input");
		this.$login_password = this.$login.find(".ac-game-settings-password input");
		this.$login_submit = this.$login.find(".ac-game-settings-submit button");
		this.$login_error_message = this.$login.find(".ac-game-settings-error-message");
		this.$login_register = this.$login.find(".ac-game-settings-option");

		this.$login.hide();

		this.$register = this.$settings.find(".ac-game-settings-register");
		this.$register_username = this.$register.find(".ac-game-settings-username input");
		this.$register_password = this.$register.find(".ac-game-settings-password-first input");
		this.$register_password_confirm = this.$register.find(".ac-game-settings-password-second input");
		this.$register_submit = this.$register.find(".ac-game-settings-submit button");
		this.$register_error_message = this.$register.find(".ac-game-settings-error-message");
		this.$register_login = this.$register.find(".ac-game-settings-option");

		this.$register.hide();

        this.$acwing_login = this.$settings.find('.ac-game-settings-acwing img');

		this.root.$ac_game.append(this.$settings);

		this.start();
	}
	start() {
		if (this.platform === "ACAPP") {
            this.getinfo_acapp();
        } else {
            this.getinfo_web();
            this.add_listening_events();
        }
	}
	add_listening_events() {
        let outer = this;

		this.add_listening_events_login();
		this.add_listening_events_register();

        this.$acwing_login.click(function() {
            outer.acwing_login();
        });
	}
	add_listening_events_login() {
		let outer = this;
		this.$login_register.click(function() {
			outer.register();   //跳到注册界面
		});
		this.$login_submit.click(function() {
			outer.login_on_remote();
		});
	}
	add_listening_events_register() {
		let outer = this;
		this.$register_login.click(function() {
			outer.login();      //跳到登录界面
		})
		this.$register_submit.click(function() {
        	outer.register_on_remote();
    	});
	}
    acwing_login() {
        $.ajax({
            url: "https://app6560.acapp.acwing.com.cn/settings/acwing/web/apply_code/",
            type: "GET",
            success: function(resp) {
                console.log(resp);
                if (resp.result === "success") {
                    window.location.replace(resp.apply_code_url);
                }
            }
        })
    }
	login_on_remote() {     //在远程服务器上登录
		let outer = this;
		let username = this.$login_username.val();
		let password = this.$login_password.val();
		this.$login_error_message.empty();
		$.ajax({
			url: "https://app6560.acapp.acwing.com.cn/settings/login/",
			type: "GET",
			data: {
				username: username,
				password: password,
			},
			success: function(resp) {
				console.log(resp);
				if (resp.result === "success") {
					location.reload();
				} else {
					outer.$login_error_message.html(resp.result);
				}
			}
		});
	}
	logout_on_remote() // 在远程服务器上登出
	{
		if (this.platform === "ACAPP") return false; // 如果在ACAPP退出就直接退出

		$.ajax({
			url: "https://app6560.acapp.acwing.com.cn/settings/logout/", // 访问url
			type: "GET",
			success: function(resp){
				console.log(resp); // 测试输出
				if (resp.result === "success")
				{
					location.reload(); // 如果成功了就直接刷新
				}
			}
		});
	}
	register_on_remote() {  //在远程服务器上注册
		let outer = this;
		let username = this.$register_username.val();
		let password = this.$register_password.val();
		let password_confirm = this.$register_password_confirm.val();
		this.$register_error_message.empty();

		$.ajax({
			url: "https://app6560.acapp.acwing.com.cn/settings/register/",
			type: "GET",
			data: {
				username: username,
				password: password,
				password_confirm: password_confirm,
			},
			success: function(resp) {
				console.log(resp);
				if (resp.result === "success") {
					location.reload();
				} else {
					outer.$register_error_message.html(resp.result);
				}
			}
		})
	}

	register() {    //打开注册界面
		this.$login.hide();
		this.$register.show();
	}
	login() {       //打开登录界面
		this.$register.hide();
		this.$login.show();
	}
	getinfo_web() {
		let outer = this;
		$.ajax({
			url: "https://app6560.acapp.acwing.com.cn/settings/getinfo/",
			type: "GET",
			data: {
				platform: outer.platform,
			},
			success: function(resp) {
				console.log(resp);
				if (resp.result === "success") {    //登录成功，关闭登录界面，打开主菜单
					outer.username = resp.username;
					outer.photo = resp.photo;
					outer.hide();
					outer.root.menu.show();
				} else {
					outer.login();
				}
			}
		});
	}
	acapp_login(appid, redirect_uri, scope, state) {
        let outer = this;
        this.root.AcWingOS.api.oauth2.authorize(appid, redirect_uri, scope, state, function(resp) {
            console.log(resp);
            if (resp.result === "success") {
                outer.username = resp.username;
                outer.photo = resp.photo;
                outer.hide();
                outer.root.menu.show();
            }
        });
    }
    getinfo_acapp() {
        let outer = this;
        $.ajax({
            url: "https://app6560.acapp.acwing.com.cn/settings/acwing/acapp/apply_code/",
            type: "GET",
            success: function(resp) {
                if (resp.result === "success") {
                    outer.acapp_login(resp.appid, resp.redirect_uri, resp.scope, resp.state);
                }
            }
        });
    }
	hide() {
		this.$settings.hide();
	}
	show() {
		this.$settings.show();
	}
}

export class AcGame {
    constructor(id, AcWingOS) {
        this.id = id;
        this.$ac_game = $('#' + id);

        this.AcWingOS = AcWingOS;   //如果是acapp端，该变量就会带着一系列y总提供的接口

        this.settings = new Settings(this);
        this.menu = new AcGameMenu(this);
        this.playground = new AcGamePlayground(this);

        this.start();
    }

    start() {
    }
}

