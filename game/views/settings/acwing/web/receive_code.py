from django.shortcuts import redirect
from django.core.cache import cache
import requests
from django.contrib.auth.models import User
from game.models.player.player import Player
from django.contrib.auth import login
from random import randint

def receive_code(request):
    data = request.GET
    code = data.get('code') # 获取Code
    state = data.get('state') # 获取state

    if not cache.has_key(state): # 如果没有这个state说明没有搞过这个state，不是这个服务器发起的暗号
        return redirect("index")
    cache.delete(state) # 删掉这个state
    
    apply_access_token_url = "https://www.acwing.com/third_party/api/oauth2/access_token/"
    params = {
        'appid': "6560",
        'secret': "5eaf6ad3a3cd4d9cb27e7e5791dee15f",
        'code': code
    }
    access_token_res = requests.get(apply_access_token_url, params=params).json() # 得到了token

    # print(access_token_res)
    access_token = access_token_res['access_token']
    openid = access_token_res['openid']

    players = Player.objects.filter(openid=openid)
    if players.exists():    ## 如果acw账户对应用户已经存在，直接登录
        login(request, players[0].user)
        return redirect("index")

    get_userinfo_url = "https://www.acwing.com/third_party/api/meta/identity/getinfo/"
    params = {
        "access_token": access_token,
        "openid": openid
    }
    userinfo_res = requests.get(get_userinfo_url, params=params).json()
    username = userinfo_res['username']
    photo = userinfo_res['photo']
    
    while User.objects.filter(username=username).exists():  ## 如果重名，额外添加数字填充
        username += str(randint(0, 9))

    user = User.objects.create(username=username)
    player = Player.objects.create(user=user, photo=photo, openid=openid)

    login(request, user)



    return redirect("index")
